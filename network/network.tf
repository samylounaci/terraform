#--------------------------------------------------------------
# This module creates all networking resources
#--------------------------------------------------------------

variable "vpc_cidr" {}
variable "vpc_name" {}
variable "azs" {}
variable "private_subnets_cidr" {}
variable "private_subnets_names" {}
variable "public_subnets_cidr" {}
variable "public_subnets_names" {}

module "vpc" {
  source = "./vpc"

  name = "${var.vpc_name}"
  cidr = "${var.vpc_cidr}"
}

module "public_subnet" {
  source = "./public_subnet"

  names    = "${var.public_subnets_names}"
  vpc_name = "${var.vpc_name}"
  vpc_id   = "${module.vpc.vpc_id}"
  cidrs    = "${var.public_subnets_cidr}"
  azs      = "${var.azs}"
}

module "nat" {
  source = "./nat"

  name              = "${var.vpc_name}-nat"
  azs               = "${var.azs}"
  public_subnet_ids = "${module.public_subnet.subnet_ids}"
}

module "private_subnet" {
  source = "./private_subnet"

  names    = "${var.private_subnets_names}"
  vpc_name = "${var.vpc_name}"
  vpc_id   = "${module.vpc.vpc_id}"
  cidrs    = "${var.private_subnets_cidr}"
  azs      = "${var.azs}"

  nat_gateway_ids = "${module.nat.nat_gateway_ids}"
}

resource "aws_network_acl" "acl" {
  vpc_id     = "${module.vpc.vpc_id}"
  subnet_ids = ["${concat(split(",", module.public_subnet.subnet_ids), split(",", module.private_subnet.subnet_ids))}"]

  ingress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  egress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags {
    Name = "${var.vpc_name}-all"
  }
}

# VPC
output "vpc_id" {
  value = "${module.vpc.vpc_id}"
}

output "vpc_cidr" {
  value = "${module.vpc.vpc_cidr}"
}

# Subnets
output "public_subnet_ids" {
  value = "${module.public_subnet.subnet_ids}"
}

output "private_subnet_ids" {
  value = "${module.private_subnet.subnet_ids}"
}

# NAT
output "nat_gateway_ids" {
  value = "${module.nat.nat_gateway_ids}"
}
