variable "application_name" {}
variable "environment" {}

resource "aws_iam_instance_profile" "eb_instance_profile" {
  name  = "InstanceProfile-${var.application_name}-${var.environment}"
  role = "${aws_iam_role.eb_instance_role.name}"
}

resource "aws_iam_role" "eb_instance_role" {
  name = "InstanceRole-${var.application_name}-${var.environment}"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

output "instance_profile"{
  value = "${aws_iam_instance_profile.eb_instance_profile.name}"
}

