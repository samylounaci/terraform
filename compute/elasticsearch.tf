resource "aws_elasticsearch_domain" "elastic_search" {
  domain_name           = "${var.es_name}"
  elasticsearch_version = "${var.es_version}"
#  access_policies

 cluster_config {
	instance_type = "${var.es_instance_type}"
	instance_count = "${var.es_instance_count}"
#	dedicated_master_enabled
#	dedicated_master_type
#	dedicated_master_count
#	zone_awareness_enabled
 	
		 }

ebs_options {
	ebs_enabled = true
	volume_size = "${var.es_volume_size}"
	volume_type = "${var.es_volume_type}"
 		}
vpc_options {

	subnet_ids = ["${element(split(",", var.es_subnets_ids), 1)}"]

}

}
