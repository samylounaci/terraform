# EFS

resource "aws_efs_file_system" "efs-app1" {
  creation_token = "${var.application_name}-${var.environment}"

  tags {
    Name = "${var.application_name}-${var.environment}"
  }
}

resource "aws_efs_mount_target" "mounttarget" {
  count          = "${length(split(",", var.azs))}"
  file_system_id = "${aws_efs_file_system.efs-app1.id}"
  subnet_id      = "${element(split(",", var.subnets_ids), count.index)}"
  security_groups = ["${aws_security_group.app-1.id}"]
}

output "efs_id" {
  value = "${aws_efs_file_system.efs-app1.id}"
}
