#resource "aws_elasticache_cluster" "${var.memcache_cluster_name}" {
resource "aws_elasticache_cluster" "memcache-cluster-1" {
  cluster_id           = "${var.memcache_id}"
  engine               = "memcached"
  node_type            = "${var.memcache_node_type}"
  num_cache_nodes      = "${var.memcache_node_number}"
  parameter_group_name = "${var.memcache_parameter_group_name}"
  port                 = "${var.memcache_port}"
}


