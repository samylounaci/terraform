#--------------------------------------------------------------
# This module creates the beanstalk stack
#--------------------------------------------------------------



# key pair
resource "aws_key_pair" "app" {
  key_name = "${var.application_name}" 
  public_key = "${var.ssh_public_key}"
}

# sec group
resource "aws_security_group" "app-1" {
  vpc_id = "${var.vpc_id}"
  name = "${var.application_name}-${var.environment}"
  description = "${var.application_name}-${var.environment} security group"
  tags {
    Name = "${var.application_name}-${var.environment}"
  }
}

resource "aws_security_group_rule" "allow-app-outgoing" {
    type = "egress"
    from_port = 0
    to_port = 0
    protocol = "-1"
    security_group_id = "${aws_security_group.app-1.id}"
    cidr_blocks = ["0.0.0.0/0"]
}

# Declaration de l'Application

resource "aws_elastic_beanstalk_application" "eurolines-app" {
  name        = "${var.application_name}"
  description = "${var.application_desc}"
}

data "aws_elastic_beanstalk_solution_stack" "phpapp" {
  most_recent   = true
  name_regex    = "^64bit Amazon Linux (.*) running PHP 7(.*)$"
}


# Declaration de l'environment

resource "aws_elastic_beanstalk_environment" "eurolines-env" {
  name                = "${var.application_name}-${var.environment}"
  application         = "${aws_elastic_beanstalk_application.eurolines-app.name}"
  solution_stack_name = "${data.aws_elastic_beanstalk_solution_stack.phpapp.name}"
  cname_prefix        = "${var.cname_prefix}-${var.environment}"

  # ..........................................................................
  # required settings

setting {
    namespace = "aws:ec2:vpc"
    name = "VPCId"
    value = "${var.vpc_id}"

 }
  setting {
    namespace = "aws:ec2:vpc"
    name = "Subnets"
    value = "${var.subnets_ids}"
  }

#####! pas de declaration de instance_profile ( existe deja sur le tenant ? )
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "IamInstanceProfile"
    value     = "${var.instance_profile}"
  }

  # ..........................................................................
  # ASG (host) settings with defaults

  setting {
      namespace = "aws:autoscaling:launchconfiguration"
      name      = "SecurityGroups"
      value     = "${aws_security_group.app-1.id}"
  }

  setting {
    namespace = "aws:ec2:vpc"
    name      = "AssociatePublicIpAddress"
    value     = "false"
  }
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "InstanceType"
    value     = "${var.asg_instance_type}"
  }

# !!!!!!!!!!!!! A verifier si elle sont cree automatiquement
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name = "EC2KeyName"
    value = "${aws_key_pair.app.id}"
  }

  setting {
    namespace = "aws:autoscaling:asg"
    name      = "MinSize"
    value     = "${var.asg_size_min}"
  }
  setting {
    namespace = "aws:autoscaling:asg"
    name      = "MaxSize"
    value     = "${var.asg_size_max}"
  }
  setting {
    namespace = "aws:autoscaling:asg"
    name      = "Availability Zones"
    value     = "${var.asg_zones}"
  }

  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "BreachDuration"
    value     = "${var.asg_trigger_breach_duration}"

  }

  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "MeasureName"
    value     = "${var.asg_trigger_measure_name}"
  }
  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "Period"
    value     = "${var.asg_trigger_period}"
  }
  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "Statistic"
    value     = "${var.asg_trigger_statistic}"
  }
  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "Unit"
    value     = "${var.asg_trigger_unit}"
  }
  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "LowerBreachScaleIncrement"
    value     = "${var.asg_trigger_lower_breach_scale_increment}"
  }
  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "UpperBreachScaleIncrement"
    value     = "${var.asg_trigger_upper_breach_scale_increment}"
  }
  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "LowerThreshold"
    value     = "${var.asg_trigger_lower_threshold}"
  }
  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "UpperThreshold"
    value     = "${var.asg_trigger_upper_threshold}"
  }

  # ..........................................................................
  # ELB settings

  setting {
    namespace = "aws:ec2:vpc"
    name      = "ELBSubnets"
    value     = "${var.elb_subnets}"
  }
  setting {
    namespace = "aws:ec2:vpc"
    name      = "ELBScheme"
    value     = "${var.elb_scheme}"
  }
  setting {
    namespace = "aws:elb:loadbalancer"
    name      = "CrossZone"
    value     = "${var.elb_cross_zone}"
  }
  setting {
    namespace = "aws:elb:policies"
    name      = "ConnectionDrainingEnabled"
    value     = "${var.elb_drain_connections}"
  }

  # ..........................................................................
  # rolling update settings

  setting {
    namespace = "aws:autoscaling:updatepolicy:rollingupdate"
    name      = "MaxBatchSize"
    value     = "${var.rupd_max_batch_size}"
  }
  setting {
    namespace = "aws:autoscaling:updatepolicy:rollingupdate"
    name      = "MinInstancesInService"
    value     = "${var.rupd_min_instances_in_service}"
  }
  setting {
    namespace = "aws:autoscaling:updatepolicy:rollingupdate"
    name      = "RollingUpdateEnabled"
    value     = "${var.rupd_enabled}"
  }
  setting {
    namespace = "aws:autoscaling:updatepolicy:rollingupdate"
    name      = "RollingUpdateType"
    value     = "${var.rupd_type}"
  }

  # ..........................................................................
  # RDS settings

  setting {
    namespace = "aws:rds:dbinstance"
    name      = "DBDeletionPolicy"
    value     = "Snapshot"
  }

  setting {
    namespace = "aws:rds:dbinstance"
    name      = "DBEngine"
    value     = "${var.db_engine}"
  }


  setting {
    namespace = "aws:rds:dbinstance"
    name      = "DBEngineVersion"
    value     = "${var.engine_version}"
  }


  setting {
    namespace = "aws:rds:dbinstance"
    name      = "DBInstanceClass"
    value     = "${var.db_instance_type}"
  }


  setting {
    namespace = "aws:rds:dbinstance"
    name      = "DBPassword"
    value     = "${var.db_passwd}"
  }


  setting {
    namespace = "aws:rds:dbinstance"
    name      = "DBAllocatedStorage"
    value     = "${var.db_sizeGB}"
  }


#  setting {
#    namespace = "aws:rds:dbinstance"
#    name      = "DBSnapshotIdentifier"
#    value     = "None"
#  }

  setting {
    namespace = "aws:rds:dbinstance"
    name      = "DBUser"
    value     = "${var.db_user}"
  }

  setting {
    namespace = "aws:rds:dbinstance"
    name      = "MultiAZDatabase"
    value     = "false"
  }

  # ..........................................................................
  # other settings

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "environment"
    value     = "${var.environment}"
  }
  setting {
    namespace = "aws:elasticbeanstalk:healthreporting:system"
    name      = "SystemType"
    value     = "enhanced"
  }
  setting {
    namespace = "aws:elasticbeanstalk:command"
    name      = "BatchSizeType"
    value     = "Fixed"
  }
  setting {
    namespace = "aws:elasticbeanstalk:command"
    name      = "BatchSize"
    value     = "1"
  }
  setting {
    namespace = "aws:elasticbeanstalk:command"
    name      = "DeploymentPolicy"
    value     = "Rolling"
  }
  tags {
    Name        = "${var.cname_prefix}-${var.environment}"
    DeployedBy  = "ScaleSquad"
    Environment = "${var.environment}"
  }

}
