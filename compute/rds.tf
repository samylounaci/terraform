# security groups
resource "aws_security_group" "rds-app-1" {
  vpc_id = "${var.vpc_id}"
  name = "rds-${var.application_name}-${var.environment}"
  description = "Allow inbound mysql traffic"
  tags {
    Name = "rds-${var.application_name}-${var.environment}"
  }
}
resource "aws_security_group_rule" "allow-mysql" {
    type = "ingress"
    from_port = 3306
    to_port = 3306
    protocol = "tcp"
    security_group_id = "${aws_security_group.rds-app-1.id}"
    source_security_group_id = "${aws_security_group.app-1.id}"
}
resource "aws_security_group_rule" "allow-rds-outgoing" {
    type = "egress"
    from_port = 0
    to_port = 0
    protocol = "-1"
    security_group_id = "${aws_security_group.rds-app-1.id}"
    cidr_blocks = ["0.0.0.0/0"]
}

# rds
resource "aws_db_instance" "rds-app-1" {
  allocated_storage    = "${var.db_sizeGB}"
  engine               = "${var.db_engine}"
  engine_version       = "${var.engine_version}"
  instance_class       = "${var.db_instance_type}"
  identifier           = "${var.application_name}-${var.environment}"
  final_snapshot_identifier = "${var.application_name}-${var.environment}"
  name                 = "${var.db_name}"
  username             = "${var.db_user}"
  password             = "${var.db_passwd}"
  db_subnet_group_name = "${aws_db_subnet_group.rds-app-1.name}"
  multi_az             = "false"
  vpc_security_group_ids = ["${aws_security_group.rds-app-1.id}"]
  storage_type         = "gp2"
  backup_retention_period = 30
  tags {
      Name = "rds-${var.application_name}-${var.environment}"
  }
}

resource "aws_db_subnet_group" "rds-app-1" {
    name = "rds-app-1"
    description = "RDS ${var.environment} subnet group"
    subnet_ids = ["${split(",",var.subnets_ids)}"]
}

