# Config

# Network
#variable "vpc_cidr" {}
#variable "vpc_name" {}
#variable "azs" {}
#variable "private_subnets_cidr" {}
#variable "private_subnets_names" {}
#variable "public_subnets_cidr" {}
#variable "public_subnets_names" {}
#variable "aws_region" {}

# Compute
# None - Static configuration


# Do not change after here
terraform {
	backend "s3" {
		bucket		= "tfstate-eurolines"
		key		= "terraform/global.tfstate"
		region		= "eu-west-1"
		profile		= "eurolines"
	}
}

provider "aws" {
	region		= "${var.aws_region}"
	profile		= "${var.aws_profile}"
}



module "network" {
	source = "./network/"

	vpc_cidr		= "${var.vpc_cidr}"
	vpc_name		= "${var.vpc_name}"
	azs			= "${var.azs}"
	private_subnets_cidr	= "${var.private_subnets_cidr}"
	private_subnets_names	= "${var.private_subnets_names}"
	public_subnets_cidr	= "${var.public_subnets_cidr}"
	public_subnets_names	= "${var.public_subnets_names}"
}




module "compute" {
	source = "./compute/"


# Network
	vpc_id              = "${module.network.vpc_id}"
	subnets_ids         = "${module.network.private_subnet_ids}"
	elb_subnets	    = "${module.network.public_subnet_ids}"
	azs		    = "${var.azs}"
	instance_profile    = "${module.iam.instance_profile}"
	ssh_public_key      = "${var.ssh_public_key}"
	application_name    = "${var.application_name}"
	application_desc    = "${var.application_desc}"
	cname_prefix        = "${var.cname_prefix}"
	environment         = "${var.environment}"
	aws_region         = "${var.aws_region}"
#BDD
	db_name             = "${var.db_name}"
	db_user	            = "${var.db_user}"
	db_passwd           = "${var.db_passwd}"
	db_sizeGB	    = "${var.db_sizeGB}"
	db_instance_type    = "${var.db_instance_type}"
	cache_instance_type = "${var.cache_instance_type}"
	db_engine               = "${var.db_engine}"
 	engine_version          = "${var.engine_version}"
	

# Auto Scalling
	asg_instance_type       = "${var.asg_instance_type}"
	asg_size_min            = "${var.asg_size_min}"
	asg_size_max            = "${var.asg_size_max}"
	asg_size_desired        = "${var.asg_size_desired}"
	asg_zones               = "${var.asg_zones}"

# MemCache
	memcache_port        = "${var.memcache_port}"
	memcache_parameter_group_name   = "${var.memcache_parameter_group_name}"
	memcache_node_number            = "${var.memcache_node_number}"
	memcache_node_type              = "${var.memcache_node_type}"
	memcache_id                     = "${var.memcache_id}"
	memcache_cluster_name           = "${var.memcache_cluster_name}"

# Elastic Search
	es_name                         = "${var.es_name}"
 	es_version                      = "${var.es_version}"
 	es_instance_type                = "${var.es_instance_type}"
 	es_instance_count               = "${var.es_instance_count}"
 	es_volume_size                  = "${var.es_volume_size}"
	es_subnets_ids         		= "${module.network.private_subnet_ids}"

}




module "iam" {
	source = "./iam/"

	application_name  = "${var.application_name}"
	environment  = "${var.environment}"
}

