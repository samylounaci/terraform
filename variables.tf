# ============================================================================
# Config
variable "aws_region" {
  default = "eu-west-1"
}

variable "aws_profile" {
  default = "default"
}

variable "ssh_public_key" {}


# ============================================================================
# Network
variable "vpc_cidr" {
  default = "10.0.0.0/16"
}

variable "vpc_name" {
  default = "vpc-1"
}

variable "azs" {
  default = "eu-west-1a,eu-west-1b"
}

variable "private_subnets_cidr" {
  default = "10.0.0.0/24,10.0.1.0/24,10.0.2.0/24"
}

variable "private_subnets_names" {
  default = "subnet-priv-eu_west_1a,subnet-priv-eu_west_1b,subnet-priv-eu_west_1c"
}

variable "public_subnets_cidr" {
  default = "10.0.10.0/24,10.0.11.0/24,10.0.12.0/24"
}

variable "public_subnets_names" {
  default = "subnet-pub-eu_west_1a,subnet-pub-eu_west_1b,subnet-pub-eu_west_1c"
}

# ============================================================================
# settings unrelated for elastic beanstalk ;)

variable "cname_prefix" {
  default = "cname"
}

variable "app_dns_name" {
  default = "elbapp"
}

variable "app_subdomain_name" {
  default = "elbtest"
}

variable "application_name" {
  default = "my-app"
}
variable "application_desc" {
  default = "my-app-desc"
}
variable "environment" {
  default = "test"
}

# ============================================================================
# required settings for RDS

variable "db_name" {
  default = "mydb"
}

variable "db_user" {
  default = "root"
}

variable "db_passwd" {
  default = "MYROOTPASSWORDHERE"
}

variable "db_sizeGB" {
  default = 10
}

variable "db_instance_type" {
  default = "t2.db.micro"
}

variable "db_engine" {
  default = "mysql"
}

variable "engine_version" {
  default = "10.1.26"
}


variable "db2_name" {
  default = "mydb2"
}

variable "db2_user" {
  default = "root"
}

variable "db2_passwd" {
  default = "MYROOTPASSWORDHERE"
}

variable "db2_sizeGB" {
  default = 10
}

variable "db2_instance_type" {
  default = "t2.db.micro"
}
# ============================================================================
# Redis Settings

variable "cache_instance_type" {
  default = "t2.cache.micro"
}

# ============================================================================
# settings for the ASG (hosts)

variable "asg_instance_type" {
  default = "t2.micro"
}

variable "asg_size_min" {
  default = "1"
}

variable "asg_size_max" {
  default = "4"
}

variable "asg_size_desired" {
  default = "2"
}

# yup, this is a thing. https://is.gd/QIH6IP
variable "asg_zones" {
  default = "Any 2"
}

# ============================================================================
# settings for scaling triggers

# Metric used for your Auto Scaling trigger.
# Valid values:
#   CPUUtilization, NetworkIn, NetworkOut, DiskWriteOps, DiskReadBytes,
#   DiskReadOps, DiskWriteBytes, Latency, RequestCount, HealthyHostCount,
#   UnhealthyHostCount
variable "asg_trigger_measure_name" {
  default = "CPUUtilization"
}

# Amount of time, in minutes, a metric can be beyond its defined limit
# (as specified in the UpperThreshold and LowerThreshold) before the trigger
# fires.
variable "asg_trigger_breach_duration" {
  default = "5"
}

# If the measurement falls below this number for the breach duration,
# a trigger is fired. (this default is for CPU usage)
variable "asg_trigger_lower_threshold" {
  default = "70"
}

# If the measurement is higher than this number for the breach duration,
# a trigger is fired. (this default is for CPU usage)
variable "asg_trigger_upper_threshold" {
  default = "85"
}

# How many Amazon EC2 instances to remove when performing a scaling activity.
variable "asg_trigger_lower_breach_scale_increment" {
  default = "-1"
}

# How many Amazon EC2 instances to add when performing a scaling activity.
variable "asg_trigger_upper_breach_scale_increment" {
  default = "2"
}

# Specifies how frequently Amazon CloudWatch measures the metrics for your
# trigger.
variable "asg_trigger_period" {
  default = "5"
}

# Statistic the trigger should use, such as Average.
# valid values: Minimum, Maximum, Sum, Average
variable "asg_trigger_statistic" {
  default = "Average"
}

# Unit for the trigger measurement, such as Bytes.
# valid values:
#     Seconds, Percent, Bytes, Bits, Count, Bytes/Second, Bits/Second,
#     Count/Second, None
variable "asg_trigger_unit" {
  default = "Percent"
}

# ============================================================================
# ELB settings

variable "elb_cross_zone" {
  default = "true"
}

variable "elb_drain_connections" {
  default = "true"
}

# ============================================================================
# rolling update settings

# The number of instances included in each batch of the rolling update.
variable "rupd_max_batch_size" {
  default = "1"
}

# The minimum number of instances that must be in service within the
# autoscaling group while other instances are terminated.
variable "rupd_min_instances_in_service" {
  default = "1"
}

# If true, enables rolling updates for an environment. Rolling updates are
# useful when you need to make small, frequent updates to your Elastic
# Beanstalk software application and you want to avoid application downtime.
# Setting this value to true automatically enables the MaxBatchSize,
# MinInstancesInService, and PauseTime options. Setting any of those options
# also automatically sets the RollingUpdateEnabled option value to true.
# Setting this option to false disables rolling updates.
variable "rupd_enabled" {
  default = "true"
}

# Time-based rolling updates apply a PauseTime between batches. Health-based
# rolling updates wait for new instances to pass health checks before moving
# on to the next batch. Immutable updates launch a full set of instances in
# a new AutoScaling group.
variable "rupd_type" {
  default = "Health"
}

# ============================================================================
# Memcache settings


variable "memcache_port" {
  default = "11211"
}

variable "memcache_parameter_group_name" {
  default = "default.memcached1.4"
}

variable "memcache_node_number" {
  default = "1"
}

variable "memcache_node_type" {
  default = "cache.m3.medium"
}

variable "memcache_id" {
  default = "default-memcache-server"
}

variable "memcache_cluster_name" {
  default = "default-memcache-cluster"
}

# N/A
# ============================================================================
# Elastic Search section

variable "es_name" {
  default = "default-es-name"
}

variable "es_version" {
  default = "5.5"
}

variable "es_instance_type" {
  default = "m4.large.elasticsearch"
}

variable "es_instance_count" {
  default = "3"
}

variable "es_volume_size" {
  default = "10"
}

variable "es_volume_type" {
  default = "gp2"
}

variable "es_subnets_ids" {
  default = "default_subnets_ids"
}

variable "es_security_groups_ids" {
  default = "default_sg"
}


