#--------------------------------------------------------------
# General
#--------------------------------------------------------------

# Config
aws_region		= "eu-west-1"
aws_profile		= "eurolines"

# Network
vpc_cidr		= "10.70.0.0/20"
vpc_name		= "vpc-eurolines-prod-eu-west-1"
azs			= "eu-west-1a,eu-west-1b,eu-west-1c"
private_subnets_cidr	= "10.70.2.0/24,10.70.6.0/24,10.70.10.0/24"
private_subnets_names	= "subnet-eurolines-prod-eu-west-1a-private,subnet-eurolines-prod-eu-west-1b-private,subnet-eurolines-prod-eu-west-1c-private"
public_subnets_cidr	= "10.70.0.0/24,10.70.4.0/24,10.70.8.0/24"
public_subnets_names	= "subnet-eurolines-prod-eu-west-1a-public,subnet-eurolines-prod-eu-west-1b-public,subnet-eurolines-prod-eu-west-1c-public"

# Beans Talk

application_name	= "eurolines"
environnement		= "env-prod"
application_desc        = "Site principal eurolines"

 # - AutoScaling Group
 asg_instance_type       = "t2.large"
 asg_size_min            = "1"
 asg_size_max            = "3"
 asg_size_desired        = "2"
 asg_zones               = "Any 3"
#
# - DNS et BDD
 cname_prefix            = "eurolines"
 db_name                 = "eurolinesdb"
 db_user                 = "root"
 db_passwd               = "zeoffzLFEKNbdeOFE"
 db_sizeGB               = "10"
 db_instance_type        = "db.t2.medium"
 cache_instance_type     = "cache.t2.medium"
 db_engine		 = "mysql"
 engine_version		 = "5.7.21"

# - Parametres Memcache
#
 memcache_port		 	 = "11211"
 memcache_parameter_group_name	 = "default.memcached1.4"
 memcache_node_number		 = "1"
 memcache_node_type		 = "cache.m3.medium"
 memcache_id		 	 = "eurolines-cache-srv"
 memcache_cluster_name		 = "eurolines-cache-srv"

# - Parametres Elastic Search
 
 es_name		 	 = "elasticsearch-eurolines"
 es_version		 	 = "5.5"
 es_instance_type		 = "m4.large.elasticsearch"
 es_instance_count		 = "3"
 es_volume_size		 	 = "10"
 es_volume_type		 	 = "gp2"

ssh_public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDviJ9O9B9zzkJx5o/Ae3Q8TkF7RmWPfHO5o5Y90tFL6DE2Fo8sUbm6w3storlXGACkrqLnOq5G+wBQmY8579qLEiNlY7NaTDNu3BgZ4spawgo4kuIw6+48w9WrKhvR7fqYujuogIdw/d8m1i+A9BdvDZjZ5vHltqmolaEAOa8J3GCR8rL9dc/QYbugqgd5Hj5sc2OS4v3zLp22Vo38+upPwdXlx2VqTDd1uafKoLCr99jdJMoHdpIIDFesk2k8J4QssH5C1y9YqWZ924PO3IcPu/k0YwK3dq+4DhoL++cpc++Cr/T8SaF3rBfK0yysJVUa50rJjsoekwk+Y5zyiGlD ScaleSquad-AWS-Account"

